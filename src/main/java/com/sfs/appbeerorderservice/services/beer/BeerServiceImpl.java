package com.sfs.appbeerorderservice.services.beer;

import com.sfs.appbeerorderservice.config.RestTemplateResponseErrorHandler;
import com.sfs.appbeerorderservice.web.dto.BeerDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;
import java.util.UUID;

@SuppressWarnings("ALL")
@ConfigurationProperties(prefix = "sf.brewery", ignoreUnknownFields = false)
@Service
@Slf4j
public class BeerServiceImpl implements BeerService {
    public final static String BEER_PATH_V1 = "/api/v1/beer/";
    public final static String BEER_UPC_PATH_V1 = "/api/v1/beerUpc/";

    private final RestTemplate restTemplate;

    private String beerServiceHost;

    public BeerServiceImpl(final RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder
                .errorHandler(new RestTemplateResponseErrorHandler())
                .build();
    }

    @Override
    public Optional<BeerDto> getBeerById(final UUID beerId) {
        if(StringUtils.hasLength(beerId.toString())){
            return Optional.of(restTemplate.getForObject(beerServiceHost + BEER_PATH_V1 + beerId.toString(), BeerDto.class));
        }
        return Optional.empty();
    }

    @Override
    public Optional<BeerDto> getBeerbyUpc(String upc) {
        if(StringUtils.hasLength(upc)) {
            return Optional.of(restTemplate.getForObject(beerServiceHost + BEER_UPC_PATH_V1 + upc, BeerDto.class));
        }
        return Optional.empty();
    }

    public void setBeerServiceHost(final String beerServiceHost) {
        this.beerServiceHost = beerServiceHost;
    }
}
