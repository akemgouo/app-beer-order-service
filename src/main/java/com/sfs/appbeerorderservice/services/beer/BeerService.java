package com.sfs.appbeerorderservice.services.beer;

import com.sfs.appbeerorderservice.web.dto.BeerDto;

import java.util.Optional;
import java.util.UUID;

public interface BeerService {

    Optional<BeerDto> getBeerById(final UUID beerId);

    Optional<BeerDto> getBeerbyUpc(final String upc);
}
