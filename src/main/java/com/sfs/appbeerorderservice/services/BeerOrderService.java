package com.sfs.appbeerorderservice.services;

import com.sfs.appbeerorderservice.web.dto.BeerOrderDto;
import com.sfs.appbeerorderservice.web.dto.BeerOrderPagedList;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface BeerOrderService {

    BeerOrderPagedList findAllBeers(final UUID customerId, final Pageable pageable);

    BeerOrderDto placeOrderBeer(final UUID customerId, final BeerOrderDto beerOrderDto);

    BeerOrderDto findOrderBeerById(final UUID customerId, final UUID orderId);

    void pickupOrderBeer(final UUID customerId, final UUID orderId);

    void deleteOrderBeer(final UUID customerId, final UUID orderId);

}
