package com.sfs.appbeerorderservice.services.Impl;

import com.sfs.appbeerorderservice.domain.Customer;
import com.sfs.appbeerorderservice.repositories.CustomerRepository;
import com.sfs.appbeerorderservice.services.CustomerService;
import com.sfs.appbeerorderservice.web.dto.CustomerPagedList;
import com.sfs.appbeerorderservice.web.mappers.CustomerMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;

    public CustomerServiceImpl(final CustomerRepository customerRepository, final CustomerMapper customerMapper) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
    }

    @Override
    public CustomerPagedList listCustomers(Pageable pageable) {
        Page<Customer> customerPage = customerRepository.findAll(pageable);
        return new CustomerPagedList(customerPage
        .stream()
        .map(customerMapper::convertEntityToDto)
        .collect(Collectors.toList()),
                PageRequest.of(customerPage.getPageable().getPageNumber(),
                        customerPage.getPageable().getPageSize()),
                customerPage.getTotalElements());
    }
}
