package com.sfs.appbeerorderservice.services.Impl;

import com.sfs.appbeerorderservice.domain.BeerOrder;
import com.sfs.appbeerorderservice.domain.BeerOrderLine;
import com.sfs.appbeerorderservice.domain.Customer;
import com.sfs.appbeerorderservice.domain.BeerOrderStatusEnum;
import com.sfs.appbeerorderservice.exceptions.InvalidOperationException;
import com.sfs.appbeerorderservice.repositories.BeerOrderLineRepository;
import com.sfs.appbeerorderservice.repositories.BeerOrderRepository;
import com.sfs.appbeerorderservice.repositories.CustomerRepository;
import com.sfs.appbeerorderservice.services.BeerOrderService;
import com.sfs.appbeerorderservice.web.dto.BeerOrderDto;
import com.sfs.appbeerorderservice.web.dto.BeerOrderPagedList;
import com.sfs.appbeerorderservice.web.mappers.BeerOrderLineMapper;
import com.sfs.appbeerorderservice.web.mappers.BeerOrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class BeerOrderServiceImpl implements BeerOrderService {


    private final BeerOrderRepository beerOrderRepository;
    private final BeerOrderLineRepository beerOrderLineRepository;
    private final CustomerRepository customerRepository;
    private final BeerOrderMapper beerOrderMapper;
    private final BeerOrderLineMapper beerOrderLineMapper;

    public BeerOrderServiceImpl(final BeerOrderRepository beerOrderRepository,
                                final BeerOrderLineRepository beerOrderLineRepository,
                                final CustomerRepository customerRepository,
                                final BeerOrderMapper beerOrderMapper,
                                final BeerOrderLineMapper beerOrderLineMapper) {
        this.beerOrderRepository = beerOrderRepository;
        this.beerOrderLineRepository = beerOrderLineRepository;
        this.customerRepository = customerRepository;
        this.beerOrderMapper = beerOrderMapper;
        this.beerOrderLineMapper = beerOrderLineMapper;
    }

    @Override
    public BeerOrderPagedList findAllBeers(final UUID customerId, final Pageable pageable) {

        Optional<Customer> customerOptional = customerRepository.findById(customerId);
        if (customerOptional.isEmpty()){
            log.warn("Client with ID {} was not found in the DB", customerId);
            throw new EntityNotFoundException("Aucun client avec l'ID" + customerId + " n'a étét trouvé dans BDD");
        }

        Page<BeerOrder> beerOrderPage = beerOrderRepository.findAllByCustomer(customerOptional.get(), pageable);
        return new BeerOrderPagedList(beerOrderPage
        .stream()
        .map(beerOrderMapper::convertEntityToDto)
        .collect(Collectors.toList()), PageRequest.of(
                beerOrderPage.getPageable().getPageNumber(),
                beerOrderPage.getPageable().getPageSize()),
                beerOrderPage.getTotalElements()
        );
    }

    @Override
    public BeerOrderDto placeOrderBeer(final UUID customerId, final BeerOrderDto beerOrderDto) {
        Optional<Customer> customerOptional = customerRepository.findById(customerId);
        if(customerOptional.isEmpty()){
            log.warn("Client with ID {} was not found in the DB", customerId);
            throw new EntityNotFoundException("Aucun client avec l'ID" + customerId + " n'a étét trouvé dans BDD");
        }

        BeerOrder beerOrder = beerOrderMapper.convertDtoToEntity(beerOrderDto);
        beerOrder.setId(null);
        beerOrder.setCustomer(customerOptional.get());
        beerOrder.setOrderStatus(BeerOrderStatusEnum.NEW);

        BeerOrder savedBeerOrder = beerOrderRepository.saveAndFlush(beerOrder);

        if(beerOrderDto.getBeerOrderLines() != null){
            beerOrderDto.getBeerOrderLines().forEach(line -> {
                BeerOrderLine beerOrderLine = beerOrderLineMapper.convertDtoToEntity(line);
                beerOrderLine.setId(null);
                beerOrderLine.setBeerOrder(savedBeerOrder);
                beerOrderLineRepository.save(beerOrderLine);
            });
        }
        log.debug("Saved Beer Order: " + beerOrder.getId());
        return beerOrderMapper.convertEntityToDto(savedBeerOrder);
    }

    @Override
    public BeerOrderDto findOrderBeerById(final UUID customerId, final UUID orderId) {
        if (customerId == null) {
            log.error("Customer is NULL");
            return null;
        }else if(orderId == null){
            log.error("Order ID is NULL");
            return null;
        }
        return beerOrderMapper.convertEntityToDto(getOrder(customerId, orderId));
    }

    @Override
    public void pickupOrderBeer(final UUID customerId, final UUID orderId) {
        BeerOrder beerOrder = getOrder(customerId, orderId);
        beerOrder.setOrderStatus(BeerOrderStatusEnum.PICKED_UP);
        beerOrderRepository.save(beerOrder);
    }

    @Override
    public void deleteOrderBeer(final UUID customerId, final UUID orderId) {
        if (orderId == null) {
            log.error("order ID is NULL");
            return;
        }
        BeerOrder beerOrder = this.getOrder(customerId,orderId);
        if(beerOrder == null){
            log.error("order is NULL");
            return;
        }
        List<BeerOrderLine> beerOrderLines = beerOrderLineRepository.findAllByBeerId(beerOrder.getId());
        if (!beerOrderLines.isEmpty()) {
            throw new InvalidOperationException("Impossible de supprimer une commande client deja utilisé");
        }
        beerOrderRepository.deleteById(orderId);
    }

    private BeerOrder getOrder(UUID customerId, UUID orderId){
        Optional<Customer> customerOptional = customerRepository.findById(customerId);
        if(customerOptional.isEmpty()){
            log.warn("Client with ID {} was not found in the DB", customerId);
            throw new EntityNotFoundException("No customer with ID = " + customerId + " found in database");
        }
        Optional<BeerOrder> beerOrderOptional = beerOrderRepository.findById(orderId);
        if(beerOrderOptional.isEmpty()){
            log.warn("Client with ID {} was not found in the DB", customerId);
            throw new EntityNotFoundException("No order with ID = " + orderId + " found in database");
        }
        BeerOrder beerOrder = beerOrderOptional.get();
        // fall to exception if customer id's do not match - order not for customer
        return beerOrder.getCustomer().getId().equals(customerId) ? beerOrder : null;
    }
}
