package com.sfs.appbeerorderservice.services;

import com.sfs.appbeerorderservice.web.dto.CustomerPagedList;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public interface CustomerService {

    CustomerPagedList listCustomers(Pageable pageable);
}
