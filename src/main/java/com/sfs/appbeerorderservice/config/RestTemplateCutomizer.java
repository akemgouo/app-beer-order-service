package com.sfs.appbeerorderservice.config;

import org.springframework.web.client.RestTemplate;

@FunctionalInterface
public interface RestTemplateCutomizer {
    void cutomize(RestTemplate restTemplate);
}
