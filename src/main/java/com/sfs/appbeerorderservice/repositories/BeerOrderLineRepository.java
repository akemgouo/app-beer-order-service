package com.sfs.appbeerorderservice.repositories;

import com.sfs.appbeerorderservice.domain.BeerOrderLine;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.UUID;

public interface BeerOrderLineRepository extends PagingAndSortingRepository<BeerOrderLine, UUID> {
    List<BeerOrderLine> findAllByBeerId(final UUID beerId);
}
