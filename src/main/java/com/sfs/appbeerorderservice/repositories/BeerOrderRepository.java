package com.sfs.appbeerorderservice.repositories;

import com.sfs.appbeerorderservice.domain.BeerOrder;
import com.sfs.appbeerorderservice.domain.Customer;
import com.sfs.appbeerorderservice.domain.BeerOrderStatusEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface BeerOrderRepository extends JpaRepository<BeerOrder, UUID> {

    Page<BeerOrder> findAllByCustomer(final Customer customer, final Pageable pageable);

    List<BeerOrder> findAllByOrderStatus(final BeerOrderStatusEnum orderStatusEnum);

}
