package com.sfs.appbeerorderservice.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Set;


@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class BeerOrder extends BaseEntity{

    private String customerRef;

    private BeerOrderStatusEnum orderStatus = BeerOrderStatusEnum.NEW;

    private String orderStatusCallBackUrl;

    @ManyToOne
    private Customer customer;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "beerOrder", cascade = CascadeType.ALL)
    private Set<BeerOrderLine> beerOrderLines;
}
