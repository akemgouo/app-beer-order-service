package com.sfs.appbeerorderservice.web.dto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
public class CustomerDto extends BaseItem {

    private String customerName;
}
