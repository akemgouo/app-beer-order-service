package com.sfs.appbeerorderservice.web.dto;

import lombok.*;

import java.util.UUID;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
public class OrderStatusUpdate extends BaseItem {
    private UUID orderId;
    private String customerRef;
    private String orderStatus;
}
