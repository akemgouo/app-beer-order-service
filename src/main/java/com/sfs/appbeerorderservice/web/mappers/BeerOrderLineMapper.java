package com.sfs.appbeerorderservice.web.mappers;

import com.sfs.appbeerorderservice.domain.BeerOrderLine;
import com.sfs.appbeerorderservice.web.dto.BeerOrderLineDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(uses = DateMapper.class, injectionStrategy = InjectionStrategy.FIELD)
@DecoratedWith(BeerOrderLineMapperDecorator.class)
public interface BeerOrderLineMapper {

    BeerOrderLineDto convertEntityToDto(final BeerOrderLine beerOrderLine);

    BeerOrderLine convertDtoToEntity(final BeerOrderLineDto beerOrderLineDto);
}
