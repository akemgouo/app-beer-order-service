package com.sfs.appbeerorderservice.web.mappers;

import com.sfs.appbeerorderservice.domain.BeerOrder;
import com.sfs.appbeerorderservice.web.dto.BeerOrderDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {DateMapper.class, BeerOrderLineMapper.class} , injectionStrategy = InjectionStrategy.FIELD)
public interface BeerOrderMapper {

    @Mapping(target = "customerId", source = "customer.id")
    BeerOrderDto convertEntityToDto(final BeerOrder beerOrder);
    BeerOrder convertDtoToEntity(final BeerOrderDto beerOrderDto);
}
