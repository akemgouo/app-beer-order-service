package com.sfs.appbeerorderservice.web.mappers;

import com.sfs.appbeerorderservice.domain.BeerOrderLine;
import com.sfs.appbeerorderservice.services.beer.BeerService;
import com.sfs.appbeerorderservice.web.dto.BeerDto;
import com.sfs.appbeerorderservice.web.dto.BeerOrderLineDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Optional;

public abstract class BeerOrderLineMapperDecorator implements BeerOrderLineMapper {

    private BeerService beerService;
    private BeerOrderLineMapper beerOrderLineMapper;

    @Autowired
    public void setBeerService(BeerService beerService) {
        this.beerService = beerService;
    }

    @Autowired
    @Qualifier("delegate")
    public void setBeerOrderLineMapper(BeerOrderLineMapper beerOrderLineMapper) {
        this.beerOrderLineMapper = beerOrderLineMapper;
    }

    @Override
    public BeerOrderLineDto convertEntityToDto(final BeerOrderLine beerOrderLine) {
        BeerOrderLineDto orderLineDto = beerOrderLineMapper.convertEntityToDto(beerOrderLine);
        Optional<BeerDto> beerDtoOptional = this.getBeerDto(beerOrderLine);
        if(beerDtoOptional.isPresent()){
            BeerDto beerDto = beerDtoOptional.get();
            orderLineDto.setBeerName(beerDto.getBeerName());
            orderLineDto.setBeerId(beerDto.getBeerId());
            orderLineDto.setBeerStyle(beerDto.getBeerStyle());
            orderLineDto.setPrice(beerDto.getPrice());
        }
        return orderLineDto;
    }

    @Override
    public BeerOrderLine convertDtoToEntity(final BeerOrderLineDto beerOrderLineDto) {
        BeerOrderLine orderLine = beerOrderLineMapper.convertDtoToEntity(beerOrderLineDto);
        Optional<BeerDto> beerDtoOptional = this.getBeerDto(orderLine);
        if(beerDtoOptional.isPresent()){
            BeerDto beerDto = beerDtoOptional.get();
            orderLine.setBeerId(beerDto.getBeerId());
        }
        return orderLine;
    }

    private Optional<BeerDto> getBeerDto(BeerOrderLine orderLine){
        Optional<BeerDto> beerDtoOptional = beerService.getBeerbyUpc(orderLine.getUpc());
        return beerDtoOptional;
    }
}
