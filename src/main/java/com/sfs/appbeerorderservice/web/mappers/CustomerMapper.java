package com.sfs.appbeerorderservice.web.mappers;

import com.sfs.appbeerorderservice.domain.Customer;
import com.sfs.appbeerorderservice.web.dto.CustomerDto;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(uses = DateMapper.class, injectionStrategy = InjectionStrategy.FIELD)
public interface CustomerMapper {

    CustomerDto convertEntityToDto(Customer customer);

    Customer convertDtoToEntity(CustomerDto customerDto);
}
