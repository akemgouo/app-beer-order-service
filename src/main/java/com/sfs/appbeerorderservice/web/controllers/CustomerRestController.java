package com.sfs.appbeerorderservice.web.controllers;

import com.sfs.appbeerorderservice.services.CustomerService;
import com.sfs.appbeerorderservice.web.controllers.api.CustomerApi;
import com.sfs.appbeerorderservice.web.dto.CustomerPagedList;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CustomerRestController implements CustomerApi {

    private static final Integer DEFAULT_PAGE_NUMBER = 0;
    private static final Integer DEFAULT_PAGE_SIZE = 25;

    private final CustomerService customerService;

    public CustomerRestController(final CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public ResponseEntity<CustomerPagedList> listCustomers(Integer pageNumber, Integer pageSize) {
        if (pageNumber == null || pageNumber < 0){
            pageNumber = DEFAULT_PAGE_NUMBER;
        }

        if (pageSize == null || pageSize < 1) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        return ResponseEntity.status(HttpStatus.OK).body(customerService.listCustomers(PageRequest.of(pageNumber,pageSize)));
    }
}
