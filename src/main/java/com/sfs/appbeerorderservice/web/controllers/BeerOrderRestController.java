package com.sfs.appbeerorderservice.web.controllers;

import com.sfs.appbeerorderservice.services.BeerOrderService;
import com.sfs.appbeerorderservice.web.controllers.api.BeerOrderApi;
import com.sfs.appbeerorderservice.web.dto.BeerOrderDto;
import com.sfs.appbeerorderservice.web.dto.BeerOrderPagedList;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@CrossOrigin
public class BeerOrderRestController implements BeerOrderApi {

    private static final Integer DEFAULT_PAGE_NUMBER = 0;
    private static final Integer DEFAULT_PAGE_SIZE = 25;

    private final BeerOrderService beerOrderService;

    public BeerOrderRestController(final BeerOrderService beerOrderService) {
        this.beerOrderService = beerOrderService;
    }

    @Override
    public ResponseEntity<BeerOrderPagedList> listOrders(final UUID customerId, Integer pageNumber, Integer pageSize) {
        if (pageNumber == null || pageNumber < 0){
            pageNumber = DEFAULT_PAGE_NUMBER;
        }

        if (pageSize == null || pageSize < 1) {
            pageSize = DEFAULT_PAGE_SIZE;
        }

        return ResponseEntity.status(HttpStatus.OK).body(beerOrderService.findAllBeers(customerId, PageRequest.of(pageNumber,pageSize)));
    }

    @Override
    public ResponseEntity<BeerOrderDto> placeOrderBeer(final UUID customerId, final BeerOrderDto beerOrderDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(beerOrderService.placeOrderBeer(customerId,beerOrderDto));
    }

    @Override
    public ResponseEntity<BeerOrderDto> getOrderBeer(UUID customerId, UUID orderId) {
        return ResponseEntity.status(HttpStatus.OK).body(beerOrderService.findOrderBeerById(customerId, orderId));
    }

    @Override
    public ResponseEntity<Void> pickupOrderBeer(UUID customerId, UUID orderId) {
        beerOrderService.pickupOrderBeer(customerId,orderId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @Override
    public ResponseEntity<Void> deleteOrderBeerById(UUID customerId, UUID orderId) {
        beerOrderService.deleteOrderBeer(customerId,orderId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
