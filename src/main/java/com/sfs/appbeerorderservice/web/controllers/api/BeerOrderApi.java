package com.sfs.appbeerorderservice.web.controllers.api;

import com.sfs.appbeerorderservice.web.dto.BeerOrderDto;
import com.sfs.appbeerorderservice.web.dto.BeerOrderPagedList;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequestMapping("/api/v1/customers/{customerId}/")
public interface BeerOrderApi {

    @GetMapping("orders")
    ResponseEntity<BeerOrderPagedList> listOrders(@PathVariable("customerId") final UUID customerId,
                                                 @RequestParam(value = "pageNumber", required = false) final Integer pageNumber,
                                                 @RequestParam(value = "pageSize", required = false) final Integer pageSize);
    @PostMapping("orders")
    ResponseEntity<BeerOrderDto> placeOrderBeer(@PathVariable("customerId") final UUID customerId, @RequestBody @Validated final BeerOrderDto beerOrderDto);

    @GetMapping("orders/{orderId}")
    ResponseEntity<BeerOrderDto> getOrderBeer(@PathVariable("customerId") final UUID customerId,@PathVariable("orderId") final UUID orderId);

    @PatchMapping("orders/{orderId}/pickup")
    ResponseEntity<Void> pickupOrderBeer(@PathVariable("customerId") final UUID customerId, @PathVariable("orderId") final UUID orderId);

    @DeleteMapping("orders/{orderId}")
    ResponseEntity<Void> deleteOrderBeerById(@PathVariable("customerId") final UUID customerId, @PathVariable("orderId") final UUID orderId);

}
