package com.sfs.appbeerorderservice.web.controllers.api;

import com.sfs.appbeerorderservice.web.dto.CustomerPagedList;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/api/v1/customers/")
public interface CustomerApi {

    @GetMapping
    ResponseEntity<CustomerPagedList> listCustomers(@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
                                                   @RequestParam(value = "pageSize", required = false) Integer pageSize);
}
