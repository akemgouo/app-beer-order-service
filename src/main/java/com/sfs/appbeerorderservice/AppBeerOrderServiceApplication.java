package com.sfs.appbeerorderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppBeerOrderServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppBeerOrderServiceApplication.class, args);
    }

}
