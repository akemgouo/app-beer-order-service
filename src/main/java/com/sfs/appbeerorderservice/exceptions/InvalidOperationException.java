package com.sfs.appbeerorderservice.exceptions;

public class InvalidOperationException extends RuntimeException {

    public InvalidOperationException() {
        super();
    }

    public InvalidOperationException(final String message) {
        super(message);
    }

    public InvalidOperationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidOperationException(final Throwable cause) {
        super(cause);
    }

    public InvalidOperationException(final Integer id) {
        super(String.valueOf(id));
    }

    public InvalidOperationException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
